﻿# Retrieves and deletes 'Downloads' folder on system

$comps = @(
"CC-1708ZG2"
"CC-16V8ZG2"
"CC-1638ZG2"
"CC-15Q9ZG2"
"CC-1785ZG2"
"CC-16Y6ZG2"
"CC-18FAZG2"
"CC-15G7ZG2"
"CC-17N3ZG2"
"CC-17S5ZG2"
"CC-1885ZG2"
"CC-17V7ZG2"
"CC-16Q3ZG2"
"CC-1863ZG2"
"CC-17H5ZG2"
"CC-1696ZG2"
"CC-15L9ZG2"
"CC-17F3ZG2"
)

ForEach ($comp in $comps){
    Get-Item "\\$comp\C$\users\*\Downloads\*" | remove-item
}