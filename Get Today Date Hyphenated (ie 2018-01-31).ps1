﻿$year =(get-date).year
$month = (get-date).month
$day = (get-date).day

if( $month -lt 10){
    Write-Host "Must add a 0, month less than 10"
    $monthfull = '0' + $month
} else {
    Write-Host "Not adding anything, date more than 10"
    $monthfull = $month
}

if( $day -lt 10){
    Write-Host "Must add a 0, day less than 10"
    $dayfull = '0' + $day
} else {
    Write-Host "Not adding anything, date more than 10"
    $dayfull = $day
}

$array = @(
    $year,
    $monthfull,
    $dayfull
)

$array -join "-"