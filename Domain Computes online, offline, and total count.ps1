# Gets computernames, tests if they are online or offline. Total count at the bottom.

$OUGroup = "OU=MYCOMPANY Computers"
$computersgroup = ((Get-ADComputer -Filter {(Enabled -eq $true)} | ? {$_.distinguishedname -like "*$OUGroup*"}).name)

$onlineCount = 0
$offlineCount = 0

Write-Host ""
Write-Host "There are"$computersgroup.count"computers in total" -Fore Green
Write-Host ""

ForEach($computer in $computersgroup) {

    $computersonline = Test-Connection -ComputerName $computer -Quiet -Count 1
    
    If ($computersonline) {
        Write-Host "There is a connection to $computer" -Fore Green
        $onlineCount++
        
    } Else {
        Write-Host "ERROR. There is no connection to $computer" -Fore Yellow
        $offlineCount++
    }
}

Write-Host $onlineCount "computers online" -Fore Green
Write-Host $offlineCount "computers offline" -Fore Yellow