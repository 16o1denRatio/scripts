﻿### Pings given IP address -> Email start of ping.
### When ping fails, emails notification.
### When ping back online, emails notification.
### Restarts loop.

# The following 2 lines ($IPAddress) and ($LocationTitle) need to be manually edited.
$IPAddress = "192.168.0.86"
$LocationTitle = "TestVM" #"Computer/device description"

# Obtains credentials
$EmailAddress = Read-Host "Email account?"
#$StoredPassword = "$env:appdata\temppass.txt"
$Get_password = Read-Host "Password?" # | out-file $StoredPassword
$password = $Get_password | ConvertTo-SecureString -AsPlainText -Force
Remove-Variable Get_password
Clear-Host
#Remove-Item $StoredPassword
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $EmailAddress, $password

#Contact list (From, To, CC)
$EmailFrom = "mcurtin@capitol-care.org"
$EmailTo = "mcurtin@capitol-care.org"
$CC = "mikecurtinwork@gmail.com"

#Subject/Body text
$time = Get-Date

$Subject_Start = "$LocationTitle @ $IPAddress - Notification system online"
$Subject_Offline = "$LocationTitle @ $IPAddress - CONNECTION HAS BEEN LOST AT $TIME!" 
$Subject_Online = "$LocationTitle @ $IPAddress - CONNECTION RE-ESTABLISHED $TIME!"

#Notifies start of script                
Send-MailMessage -From $Emailfrom -to $EmailTo -Cc $cc -Subject $Subject_Start -SmtpServer "smtp.gmail.com"  -Port 587 -UseSsl -Body $Subject_Start -Credential $cred

# Start of monitoring loop
Do {

    # Starts Test-NetConnection, looking for failed pings
    Do {

        $PingComputer1 = (Test-NetConnection $IPAddress).PingSucceeded
        $PingComputer2 = (Test-NetConnection $IPAddress).PingSucceeded

        $PingComputer1
        $PingComputer2

        Sleep 10
    }

    # When ping fails, it will send a notification email.
    Until (($PingComputer1 -eq $false) -and ($PingComputer2 -eq $false))

    # Send email notification that connection has been lost
    Send-MailMessage -From $Emailfrom -to $EmailTo  -Cc $cc -Subject $Subject_Offline -SmtpServer "smtp.gmail.com"  -Port 587 -UseSsl -Body $Subject_Start -Credential $cred

    Do {
        $PingComputer1 = (Test-NetConnection $IPAddress).PingSucceeded
        $PingComputer2 = (Test-NetConnection $IPAddress).PingSucceeded

        $PingComputer1
        $PingComputer2

        Sleep 10
    }

    # When ping is successful, it will send a notification email.
    Until (($PingComputer1 -eq $true) -and ($PingComputer2 -eq $true))

    # Send email notification that connection is back online
    Send-MailMessage -From $Emailfrom -to $EmailTo -Cc $cc -Subject $Subject_Online -SmtpServer "smtp.gmail.com"  -Port 587 -UseSsl -Body $Subject_Start -Credential $cred
}

Until (1 -eq 0) # end of monitoring loop, which never terminates.