﻿$hosts = @(
#17 GH/SA desktops
"CC-1638ZG2"
"CC-1885ZG2"
"CC-1785ZG2"
"CC-17H5ZG2"
"CC-16Y6ZG2"
"CC-15L9ZG2"
"CC-15G7ZG2"
"CC-1863ZG2"
"CC-15Q9ZG2"
"CC-16Q3ZG2"
"CC-16V8ZG2"
"CC-17F3ZG2"
"CC-17V7ZG2"
"CC-1696ZG2"
"CC-17S5ZG2"
"CC-1708ZG2"
"CC-17N3ZG2"

#Accuflo and SLP - not deployed
"CC-17X7ZG2"
"CC-18FAZG2"
)

foreach($host in $hosts) {

Write-Host ""
Write-Host $host":"
Write-Host ""

If (Test-connection -quiet -count 1 -computername $host) {
Get-ChildItem \\$host\C$\users\GH*\Downloads\*.*
Get-ChildItem \\$host\C$\users\GH*\Pictures\*.*
} else {
Write-Host "Cannot connect to $host" -fore DarkYellow
}

}