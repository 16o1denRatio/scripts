﻿<#
Install-PackageProvider -Name NuGet -Verbose

	cd C:\Windows\System32\
	Mkdir C:\Dell
	Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force | more
	
	Save-Module -Name DellBIOSProvider -Path C:\Dell | more
	Install-Module -name dellbiosprovider -Force | more
	import-Module -Name DellBIOSProvider | more
	Get-Item -Path DellSmbios:\Security\IsAdminPasswordSet | more
	
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
#>

# Does not work if tight OpenDNS policy is applied to logged in user (Currently part of user group Lockdown_Group)
Get-PSRepository
Set-PSRepository PSGallery -InstallationPolicy Trusted
find-module "*DellBiosProvider"

Save-Module -Name DellBIOSProvider -Path C:\Dell
Install-Module -name dellbiosprovider -Force
import-Module -Name DellBIOSProvider

Get-Item -Path DellSmbios:\Security\IsAdminPasswordSet