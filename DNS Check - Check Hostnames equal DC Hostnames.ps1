﻿# Verifies DNS IP Address's A Record is equal to the actual hostname of the machine.
# Script was originally created because stale entries in DNS would identify itself as a different machine, in regards to VPN network IP.

$comps = @(
#17 GH/SA desktops
"CC-17H5ZG2" # GH2109
"CC-15G7ZG2" # GH1538
"CC-16V8ZG2" # GH1722
"CC-1638ZG2" # GH1638
"CC-1885ZG2" # SA1426
"CC-1785ZG2" # GH1986
"CC-16Y6ZG2" # GH1831
"CC-15L9ZG2" # GH1510
"CC-1863ZG2" # GH1592
"CC-17S5ZG2" # SA1213
"CC-1696ZG2" # GH1640
"CC-17V7ZG2" # SA1394
"CC-17F3ZG2" # GH2027
"CC-16Q3ZG2" # GH1701
"CC-15Q9ZG2" # GH1545
"CC-17N3ZG2" # GH2223
"CC-1708ZG2" # GH1960

#Accuflo and SLP - not deployed
#"CC-17X7ZG2"
#"CC-18FAZG2"
)

foreach($comp in $comps) {

Write-Host ""
Write-Host ""
Write-Host ""
Write-Host $comp":"

    If (Test-connection -quiet -count 1 -computername $comp) {

        $RemoteHostName = Invoke-command -ComputerName $comp -scriptblock { hostname }
        $WhatsMyIPLocal = Invoke-command -ComputerName $comp -scriptblock { (Get-NetIPConfiguration Ca*).IPv4Address.Ipaddress }

            If($RemoteHostName -eq $comp) {

                Write-Host "The hostname entry is correct!" -Fore Green
                Write-Host "Remote computer says: $RemoteHostName is my hostname" -Fore Green
                Write-Host "Domain Controller says: $comp is the hostname" -Fore Green
                Write-Host "My VPN IP address is $WhatsMyIPLocal."

            } else {

                Write-Host "DNS entries are incorrect!" -Fore Yellow

            }

    } else {

    Write-Host "Cannot connect to $comp. Host is offline." -fore DarkYellow

    }
}
