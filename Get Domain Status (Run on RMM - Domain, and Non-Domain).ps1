﻿# Gets domain status, appends to single file on DC. This is to be run on RMM (domain-joined AND local workstation).

function DomainStatus {

$stamp = (get-date).ToString("MM-dd-yy")

    Write-Output "---"
    $hostname = hostname
    Get-Date
    $DomainStatus = (Get-WmiObject -class Win32_computerSystem).domain

    ($DomainStatus, $hostname) -join ", "
}

DomainStatus | Out-File -Append \\192.168.0.202\inventory\DomainStatus$stamp.txt