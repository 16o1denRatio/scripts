﻿# Pulls a list of AD Computers, list of hostnames
$comps = (Get-ADComputer -Filter {enabled -eq $true}).name
$item = "*Konica Scan*.lnk"

# Searches all C:\users for that item, removes the item
ForEach ($comp in $comps){
    Get-Item "\\$comp\C$\users\*\Desktop\$item" | remove-item
}