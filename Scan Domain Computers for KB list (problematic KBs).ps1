﻿# Scans all Domain computers, and compares them with a KB list. In this case, the KBs needed to be found to be removed.

# Pulls (domain admin) credentials.
$cred = Get-Credential

# Creates a list of AD Computers, only enabled.
$comps = (Get-ADComputer -Filter {enabled -eq $true}).name

# For every AD Computer, list computer name, and invoke the command to search for the updates.
ForEach($comp in $comps){
    Write-Host $comp -ForegroundColor Green
    Invoke-command -ComputerName "$comp" -Credential $cred -ScriptBlock {
        $KBList = @(
            "KB4099633",
            "KB4103718",
            "KB4033342",
            "KB890830"
            #"KB2693643"# this last one as a test
            ) 

        ForEach ($KB in $KBList){
            
            If(get-hotfix | where HotFixID -Like $KB){
                Write-Host "Found KB $KB that needs to be removed off from $comp!"
            }
        }
    }
}