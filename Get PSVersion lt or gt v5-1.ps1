﻿$FullPSVersion = $PSVersionTable.PSVersion

$major = $FullPSVersion.major 
$minor = $FullPSVersion.minor

$Required = 5.1

If($Major,$Minor -join "." -lt $required){
    Write-Host "PS Version is less than $required need to do X, Y, and Z"

} else {
    Write-Host "PS Version is $required. No need to do anything"
}