#
# This script does the following:
#   -Uninstalls MS Office 2016 programs
#   -Removes Windows 10 Apps
#   -Cleans up Start Menu

# Uninstall Office 16 Click-to-Run Licensing Component
msiexec /x "{90160000-008F-0000-1000-0000000FF1CE}" /passive

# Uninstall Office 16 Click-to-Run Extensibility Component 64-bit Registration
msiexec /x "{90160000-00DD-0000-1000-0000000FF1CE}" /passive

# Uninstall MS Office HomeStudentRetail.16_en-us
start-process -filepath "C:\Program Files\Common Files\Microsoft Shared\ClickToRun\OfficeClickToRun.exe" -argumentlist "scenario=install scenariosubtype=ARP sourcetype=None productstoremove=HomeStudentRetail.16_en-us_x-none culture=en-us" -wait

# Uninstall MS Office HomeStudentRetail.16_es-es
start-process -filepath "C:\Program Files\Common Files\Microsoft Shared\ClickToRun\OfficeClickToRun.exe" -argumentlist "scenario=install scenariosubtype=ARP sourcetype=None productstoremove=HomeStudentRetail.16_es-es_x-none culture=es-es" -wait

# Uninstall MS Office HomeStudentRetail.16_fr-fr
start-process -filepath "C:\Program Files\Common Files\Microsoft Shared\ClickToRun\OfficeClickToRun.exe" -argumentlist "scenario=install scenariosubtype=ARP sourcetype=None productstoremove=HomeStudentRetail.16_fr-fr_x-none culture=fr-fr" -wait

#Remove unnecessary apps.
$apps = @(
"Microsoft.Office.OneNote"
"Microsoft.MicrosoftOfficeHub"
"0D16BB98.Houzz"
"9E2F88E3.Twitter"
"Microsoft.MicrosoftSolitaireCollection"
"flaregamesGmbH.RoyalRevolt2"
"king.com.CandyCrushSodaSaga"
"Microsoft.MinecraftUWP"
"Facebook.Facebook"
"Microsoft.SkypeApp"
"Microsoft.BingWeather"
"Microsoft.BingNews"
"Microsoft.XBoxApp"
# Does not uninstall properly; never has - "Microsoft.XboxGameCallableUI"
"Microsoft.Messaging"
"Microsoft.ZuneVideo"
"Microsoft.WindowsMaps"
"DB6EA5DB.CyberLinkMediaSuiteEssentials"
# Does not uninstall properly; never has - "Microsoft.XboxIdentityProvider"
"Microsoft.MicrosoftStickyNotes"
"Microsoft.3DBuilder"
"Microsoft.ZuneMusic"
"Microsoft.Windows.Photos"
"Microsoft.People"
)

ForEach ($app in $apps) {
Get-AppxPackage -Name $app -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | Where-Object DisplayName -EQ $app | Remove-AppxProvisionedPackage -Online
}

# Remove shortcut items in Start Menu
Remove-Item "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\System Tools\Create USB Recovery.lnk"
Remove-Item "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Dell\SupportAssist\SupportAssist.lnk"
Remove-Item "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\CyberLink Media Suite\CyberLink Media Suite 12.lnk"
Remove-Item "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Excel 2016.lnk"
Remove-Item "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Word 2016.lnk"
Remove-Item "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\OneNote 2016.lnk"
Remove-Item "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\PowerPoint 2016.lnk"

# Pull program GUID/Full name:
# Get-WmiObject -Class Win32_Product -ComputerName .
# 
# $app = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -match "Software Name"
# }
# 
# $app.Uninstall()
