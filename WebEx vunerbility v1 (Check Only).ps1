﻿# Continue on error
$ErrorActionPreference= 'silentlycontinue'

# Pull list of AD Computers
$comps = (get-adcomputer -filter {enabled -eq $true}).name

# If computer is online, scans if file exists.
ForEach($comp in $comps) {
    If(Test-NetConnection){
        Invoke-command -ComputerName $comp -ScriptBlock {
            $hostname = hostname 
            If(Test-Path "C:\ProgramData\Webex\ieatgpc.dll"){
                Write-Host "We need to remove or update WebEx on computer $hostname" -Fore Red
            } else {
                Write-Host "WebEx is not installed on $hostname; all set" -Fore Green
            }
        }
    } else {
        Write-Host "$comp is offline; nothing to do" -fore yellow
    }
}