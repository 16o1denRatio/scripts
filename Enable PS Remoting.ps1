# Add TrustedHosts
winrm quickconfig -force
set-item wsman:\localhost\Client\TrustedHosts -value * -force

# Remote WMI
netsh advfirewall firewall set rule group="Windows Management Instrumentation (WMI)" new enable=yes

# Turn on Network Sharing/Network Discovery
netsh firewall set service type=fileandprint mode=enable profile=all
netsh advfirewall firewall set rule group="network discovery" new enable=yes

# and Enabling PS-Remoting
Enable-PSRemoting