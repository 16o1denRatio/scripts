﻿# Continue on error
$ErrorActionPreference= 'silentlycontinue'

# Pull list of AD Computers
$comps = (get-adcomputer -filter {enabled -eq $true}).name

# If computer is online, scans if file exists AND removes/uninstalls.
ForEach($comp in $comps) {
    If(Test-NetConnection){
        Invoke-command -ComputerName $comp -ScriptBlock {
            $hostname = hostname 

            If(Test-Path "C:\programdata\webex\ieatgpc.dll"){
                Write-Host "We need to remove or update WebEx on computer. $hostname" -Fore DarkYellow
                Write-Host "removing... This will take about 60 seconds." -Fore DarkYellow

                Start-Process "c:\ProgramData\Webex\atcliun.exe" -ArgumentList "/v_meet /v_ra /v_smt"
                Sleep 60
                get-process atcliun | stop-Process

                If (Test-Path "C:\ProgramData\Webex\ieatgpc.dll"){
                    Write-Host "Program still installed. Please remove/update manually." -Fore Red
                } else {
                    Write-Host "WebEx removed from computer $comp" -fore Green
                }
            } else {
                Write-Host "WebEx is not installed on $hostname; all set." -Fore Green
            }
        }
    } else {
        Write-Host "$comp is offline; nothing to do" -fore yellow
    }
}
