﻿# Installs all Domain-shared printers on given workstation. This was made Pre-Active Directory

$computers = @(
"CC-1638ZG2"
"CC-1885ZG2"
"CC-1785ZG2"
"CC-17H5ZG2"
"CC-16Y6ZG2"
"CC-15L9ZG2"
"CC-17X7ZG2"
"CC-15G7ZG2"
"CC-1863ZG2"
"CC-15Q9ZG2"
"CC-16Q3ZG2"
"CC-16V8ZG2"
"CC-17F3ZG2"
"CC-17V7ZG2"
"CC-1696ZG2"
"CC-17S5ZG2"
"CC-1708ZG2"
"CC-17N3ZG2"
"CC-18FAZG2"
),(
"GH1638",
"SA1426",
"GH1986",
"GH2109",
"GH1831",
"GH1510",
"Oakwood SLP",
"GH1538",
"GH1592",
"GH1545",
"GH1701",
"GH1722",
"GH2027",
"SA1394",
"GH1640",
"SA1213",
"GH1960",
"GH2223",
"Accuflow"
)

$elapsed = [System.Diagnostics.Stopwatch]::StartNew()
write-host "Started at $(get-date)" -Fore Green
Write-Host "Please wait for the script to complete running."
Write-Host "This should take about 10 minutes..."
#Write-Host "Please be patient..." -Fore Yellow
#Write-Host "...Cup Ramen is patient. :)" -Fore Yellow

For ($i=0; $i -lt 19; $i++) {

    $computerHostName = $computers[0][$i]
    $computerPrinterName = $computers[1][$i]

    # Installs all printers

    rundll32 printui.dll PrintUIEntry /in /n "\\$computerHostName\$computerPrinterName HP M227" /q
    
    # Write-Host "Computername"$computers[0][$i] "Printer" $computers[1][$i]
    
    Sleep -Seconds 5
    
}

Sleep 120

For ($i=0; $i -lt 19; $i++) {

    $computerHostName = $computers[0][$i]
    $computerPrinterName = $computers[1][$i]
    $table = get-printer
    
    If($table.name | select-string -pattern "$computerHostName") {
    
        Write-Host "Computername"$computers[0][$i] "Printer" $computers[1][$i] -Fore Green
        
    } else {
        
        Write-Host "$computerhostName\$ComputerPrinterName does not exist!" -Fore Yellow
        
    }
}

Write-Host "Ended at $(get-date)" -Fore Green
Write-Host "Total Elapsed Time: $($elapsed.Elapsed.ToString())"