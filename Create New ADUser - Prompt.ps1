﻿# Creates username with first initial + last name (FLast). Adds user to "DOMAIN/UNASSIGNED USERS". Pulls up dsa.msc to manage user

$FirstName = Read-Host "First Name";
$LastName = Read-Host "Last Name";
$Password = Read-Host "Password";

$UserName = $FirstName[0] + $LastName

$AddAUser = New-ADUser -AccountPassword (ConvertTo-SecureString $Password -AsPlainText -Force) -Name "$Firstname $Lastname" -SamAccountName $UserName -UserPrincipalName $USerName -Surname $LastName -GivenName $FirstName -Path "OU=unassigned users,dc=CCSCP,dc=com" -Enabled $true

$ADUserObject = Get-ADUser $username

Write-Host ""
Write-Host "New user "
Write-Host $UserName -Fore Green
Write-Host "has been created in the group"
Write-Host $ADUserObject.DistinguishedName -Fore Green
Write-Host "They have been added as a 'Member Of'"
Write-Host "RDS_users" -Fore Green

$group = "RDS_users"
Add-ADGroupMember $group $UserName

Write-Host "Please move the user from [OU=UNASSIGNED USERS] into the correct OU." -Fore Yellow
pause 5

dsa.msc