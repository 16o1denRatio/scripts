﻿# Stops the CCLeaner process on all Domain Computers. Removed becuase of CCleaner security breach. Will only install if needed on a one-by-one basis.

$computers = (Get-ADComputer -Filter {enabled -eq $true}).name

ForEach($computer in $computers){

    $ping = Test-Connection -count 1 -Computername $computer -quiet

        If($ping -eq $true){

            Write-Host "$computer - status of CCleaner running...:" -fore green

            Invoke-Command -ComputerName $computer {

                $Program = "CCleaner"
                $TestPath = Test-Path "C:\Program Files\$Program\"

                 Get-Process  | where ProcessName -like "*ccleaner*"
            
                 If($TestPath -eq $true) {

                    Write-Host "$Program folder still exists! ... Removing" -Fore "Yellow"
                    Remove-Item -Recurse -Force "C:\Program Files\$Program"
                
                 } Else {

                    Write-Host "$Program does not exist, nothing to worry about" -Fore Green

                 }

                 $TestPathAgain = Test-Path "\\$computer\C$\Program Files\CCleaner\"

                  If($TestPathAgain -eq $true) {

                       Write-Host "Oh no! File(s) did not delete properly!" -Fore Red
                                
                  } Else {

                       Write-Host "$Program is gone" -Fore Green

                  }
       
            }
        } Else {
    
        Write-Host "We cannot connect to the computer $computer"

        }
}



