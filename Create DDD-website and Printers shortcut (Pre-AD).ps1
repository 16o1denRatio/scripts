﻿# This script creates the DDD website and Printers shortcut on the Public Desktop so that all users have these two shortcuts.

# This if the variable that will hold the computer name of your target device
$computer = hostname
# This command will create the shortcut object
$WshShell = New-Object -ComObject WScript.Shell
# This is where the shortcut will be created
$Shortcut = $WshShell.CreateShortcut("\\$computer\C$\Users\Public\Desktop\DDD Website.lnk")
# This is the program the shortcut will open
$Shortcut.TargetPath = "C:\Program Files (x86)\Internet Explorer\iexplore.exe"
# This is the icon location that the shortcut will use
##$Shortcut.IconLocation = "C:\AwesomeIcon.ico,0"
# This is any extra parameters that the shortcut may have. For example, opening to a google.com when internet explorer opens
$Shortcut.Arguments = "https://ddcmis.dhs.state.nj.us"
# This command will save all the modifications to the newly created shortcut.
$Shortcut.Save()

# Quick shortcut creation script

# This if the variable that will hold the computer name of your target device
$computer = hostname
# This command will create the shortcut object
$WshShell = New-Object -ComObject WScript.Shell
# This is where the shortcut will be created
$Shortcut = $WshShell.CreateShortcut("\\$computer\C$\Users\Public\Desktop\Printers.lnk")
# This is the program the shortcut will open
$Shortcut.TargetPath = "C:\windows\system32\control.exe"
# This is the icon location that the shortcut will use
##$Shortcut.IconLocation = "C:\AwesomeIcon.ico,0"
# This is any extra parameters that the shortcut may have. For example, opening to a google.com when internet explorer opens
$Shortcut.Arguments = "printers"
# This command will save all the modifications to the newly created shortcut.
$Shortcut.Save()