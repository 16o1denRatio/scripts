﻿# Gets last reboot time of computer(hostname)

Write-Host "When prompted, please use DOMAIN\USER credentials!" -fore green
$servername = Read-Host "What server do you want to connect to?"
$cred = Get-Credential

Invoke-Command -ComputerName $servername -Credential $cred -scriptblock { Get-EventLog System | Where {$_.EventID -eq  1074} | Select-Object -first 5 }

pause   